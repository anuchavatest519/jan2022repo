import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetExample {

	@Test
	public void GetDetails()
	{
		RestAssured.baseURI="https://reqres.in/api";
		RequestSpecification httpRequest=RestAssured.given();
		Response response=httpRequest.get("/users");
		int statusLine=response.getStatusCode();
		Assert.assertEquals(statusLine,200,"Correct Status");
	}
	
}
