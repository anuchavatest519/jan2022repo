package com.pompages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//this class will store all the locators and 
public class LoginPage {
  
   WebDriver driver;
   By username=By.id("user_login");
   By pwd=By.xpath("//input[@id='user_pass']");
   By loginBtn=By.cssSelector("#wp-submit");
   
   public LoginPage(WebDriver driver)
   {
   this.driver=driver;
   }
   
   public void typeUserName()
   {
        driver.findElement(username).sendKeys("admin");
   }
   public void typePassword()
   {
	   driver.findElement(pwd).sendKeys("demo123");
   }
   public void typeLoginBtn()
   {
	   driver.findElement(loginBtn).click();
   }
}
