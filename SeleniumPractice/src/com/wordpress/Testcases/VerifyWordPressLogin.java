package com.wordpress.Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import com.pompages.LoginPage;

public class VerifyWordPressLogin {
	
	
	@Test
	public void verifyValidLogin()
	{
		System.setProperty("webdriver.gecko.driver","C:\\Program Files\\Selenium\\geckodriver.exe");
      WebDriver driver=new FirefoxDriver();
      driver.manage().window().maximize();
      driver.get("https://login.wordpress.org/?locale=en_US");
      LoginPage login=new LoginPage(driver);
      login.typeUserName();
      login.typePassword();
      login.typeLoginBtn();
      driver.quit();
      
}
}
