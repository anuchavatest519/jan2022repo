package SeleniumFirstPac;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromePractice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Selenium\\chromedriver.exe");
		//ChromeDriver driver=new ChromeDriver();
		//driver.get("www.amazon.com");
		 WebDriver driver = new ChromeDriver();
	      //implicit wait
	      driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	      //launch URL
	      driver.get("https://www.tutorialspoint.com/about/about_careers.htm");
	      System.out.println("Page title is: " + driver.getTitle());
	      driver.quit();

	}

}
