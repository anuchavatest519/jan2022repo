package SeleniumFirstPac;	
import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.*;
	import org.openqa.selenium.firefox.FirefoxDriver;

	public class RadioButton {
	public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.gecko.driver","C:\\Program Files\\Selenium\\geckodriver.exe");
	WebDriver driver = new FirefoxDriver();

	 driver.get("https://rahulshettyacademy.com/AutomationPractice/");   
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
     driver.manage().window().maximize();
     driver.findElement(By.xpath("//input[@value='radio1']")).click();
 	 System.out.println("The Output of the IsSelected " + driver.findElement(By.xpath("//input[@value='radio1']")).isSelected());
 	 System.out.println("The Output of the IsEnabled " + driver.findElement(By.xpath("//input[@value='radio1']")).isEnabled());
 	 System.out.println("The Output of the IsDisplayed " + driver.findElement(By.xpath("//input[@value='radio1']")).isDisplayed());
	 
 	driver.findElement(By.xpath("//input[@value='radio2']")).click();
 	System.out.println("The Output of the IsSelected " + driver.findElement(By.xpath("//input[@value='radio1']")).isSelected());
	 System.out.println("The Output of the IsEnabled " + driver.findElement(By.xpath("//input[@value='radio1']")).isEnabled());
	 System.out.println("The Output of the IsDisplayed " + driver.findElement(By.xpath("//input[@value='radio1']")).isDisplayed());
	 
 	driver.close();
	 
	   }
	}

