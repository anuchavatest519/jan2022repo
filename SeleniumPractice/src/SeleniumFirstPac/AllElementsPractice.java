package SeleniumFirstPac;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class AllElementsPractice {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Selenium\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/AutomationPractice/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		/*
		 *Checking img element using CssSelector
		 * driver.findElement(By.cssSelector(".logoClass")).click();
		 * System.out.println("success");
		 *  driver.navigate().back();
		 *   String s=driver.getCurrentUrl(); 
		 *   System.out.println(s);
		 * driver.findElement(By.xpath("//img[@class='logoClass']")).click(); 
		 * String s1=driver.getCurrentUrl(); System.out.println(s1);
		 */
        //Checking how to get defaulttextbox value 
		/*
		 * WebElement text= driver.findElement(By.id("autocomplete")); String
		 * s2=text.getAttribute("placeholder"); System.out.println(s2);
		 */
       //getting textbox entered value
		/*
		 * text.sendKeys("Hello"); String s3= text.getAttribute("value");
		 * System.out.println(s3);
		 */
       //Dropdown example
		/*
		 * Select sel=new Select(driver.findElement(By.id("dropdown-class-example")));
		 * sel.selectByVisibleText("Option1");
		 */
       //handling alert
		/*
		 * driver.findElement(By.id("alertbtn")).click(); Thread.sleep(3000);
		 * driver.switchTo().alert().accept();
		 * driver.findElement(By.id("confirmbtn")).click(); Thread.sleep(3000);
		 * System.out.println(driver.switchTo().alert().getText());
		 * driver.switchTo().alert().dismiss();
		 */        
        //Switch between windows
		/*
		 * String mainwin=driver.getWindowHandle(); 
		 * System.out.println("Main Window "+mainwin );
		 *  driver.findElement(By.id("openwindow")).click();
		 * Thread.sleep(20000); 
		 * Set<String> allhandles=driver.getWindowHandles();
		 * System.out.println(allhandles);
		 *  System.out.println("all windows " + allhandles.size());
		 *  for (String windowHandle: allhandles)
		 *   {
		 * if(mainwin.equals(windowHandle)) 
		 * {
		 *  System.out.println(" Window "+windowHandle +"\n URL "+driver.getCurrentUrl() +"\n Title" +driver.getTitle()); } else {
		 * System.out.println(windowHandle); 
		 * driver.switchTo().window(windowHandle);
		 * System.out.println(" Window "+windowHandle +"\n URL "+driver.getCurrentUrl() +"\n Title" +driver.getTitle()); } }
		 */        
       //switch tab---didnt work
		/*
		 * System.out.println(driver.getTitle());
		 * driver.findElement(By.id("opentab")).click();
		 *  Thread.sleep(3000);
		 * 
		 * Actions action= new Actions(driver);
		 * action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
		 * 
		 * System.out.println(driver.getTitle());
		 */
        
	}

}
