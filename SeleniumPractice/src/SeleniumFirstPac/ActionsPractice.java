package SeleniumFirstPac;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class ActionsPractice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Selenium\\chromedriver.exe");
      WebDriver driver=new ChromeDriver();
      
      driver.get("https://www.facebook.com");
      driver.manage().window().maximize();
      driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
      WebElement username=driver.findElement(By.id("email"));
      Actions builder=new Actions(driver);
     Action action=builder.moveToElement(username)
    		 .click()
    		 .keyDown(username,Keys.SHIFT)
    		 .sendKeys("anusha")
    		 .keyUp(username,Keys.SHIFT)
    		 .doubleClick()
    		 .contextClick()
    		 .build();
    		 action.perform();
      
	}

}
