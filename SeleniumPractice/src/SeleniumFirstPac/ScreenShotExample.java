package SeleniumFirstPac;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class ScreenShotExample {
   
	static WebDriver driver;
	public static void main(String[] args)  throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Selenium\\chromedriver.exe");
		 driver=new ChromeDriver();
		driver.get("http://www.facebook.com");
		
	// create a object for file
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src,new File("C:\\Users\\abhiv\\Desktop\\Testing Assignments\\screenshot.png"));
		driver.close();

	}

}
