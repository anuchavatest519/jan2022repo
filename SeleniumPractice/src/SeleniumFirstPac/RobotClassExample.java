package SeleniumFirstPac;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class RobotClassExample {

	public static void main(String[] args) throws AWTException, InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Selenium\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.edureka.co/");
		//driver.manage().window().maximize();
		driver.findElement(By.linkText("Courses")).click();
		Thread.sleep(3000);
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_DOWN);	
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_TAB);
		System.out.println("success 1st tab");
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_TAB);
		System.out.println("success 2nd tab");
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_TAB);
		System.out.println("success 3rd tab");
		Thread.sleep(3000);
		robot.mouseMove(30,100);
		Thread.sleep(5000);
		driver.quit();
	}

}
