package SeleniumFirstPac;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumDataFPage {

	public static void main(String[] args) throws InterruptedException {
		//TODO Auto-generated method stub
        System.setProperty("webdriver.gecko.driver","C:\\Program Files\\Selenium\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.w3schools.com");
        driver.manage().window().maximize();
        //Thread.sleep(1000);
        //Selecting Tutorials
        driver.findElement(By.id("navbtn_tutorials")).click();
        //Thread.sleep(1000);
        //navigating to HTML course
        driver.findElement(By.linkText("Learn HTML")).click(); 
        driver.findElement(By.linkText("HTML Tables")).click();
        //driver.close();
        // /html/body/div[7]/div[1]/div[1]/div[3]/div/table/tbody/tr[2]/td[3]
        String s= driver.findElement(By.xpath("//table[@id='customers']/tbody/tr[2]/td[3]")).getText();
        System.out.println("Country Name from Table is "+s);
        driver.close();
        
	}

}
