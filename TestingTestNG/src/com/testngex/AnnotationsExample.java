package com.testngex;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class AnnotationsExample {

	@Test
	void methodName1()
	{
		System.out.println("First case 1");
	}
	@Test
	void methodName()
	{
		System.out.println("First case");
	}
	@BeforeSuite
	void methodBeforeSuit()
	{
		System.out.println("Before suit");
	}
	@BeforeTest
	void methodBeforeTest()
	{
		System.out.println("Before Test");
	}
	@BeforeClass
	void methodBeforeClass()
	{
		System.out.println("Before Class");
	}
	@BeforeMethod
	void methodBeforeMethod()
	{
		System.out.println("Before method");
	}
	@AfterMethod
	void methodAfterMethod()
	{
		System.out.println("After method");
	}
	@AfterTest
	void methodAfterTest()
	{
		System.out.println("After test");
	}
	@AfterClass
	void methodAfterClass()
	{
		System.out.println("After class");
	}
	@AfterSuite
	void methodAfterSuite()
	{
		System.out.println("After Suite");
	}
	@Test
	void methodRepeatMethod()
	{
		System.out.println("Repeat method");
	}

	
}
