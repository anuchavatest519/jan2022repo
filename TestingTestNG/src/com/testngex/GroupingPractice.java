package com.testngex;

import org.testng.annotations.Test;

public class GroupingPractice {

	@Test (groups= {"smokeTest","FunctionalTest"})
	void sampleMethod()
	{
		System.out.println("Sample Method");
	}
}
