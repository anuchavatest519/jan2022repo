import java.util.*;
public class RecurExample {

	
	    public static void main(String[] args)
	    {
	        printNos(1, 10);
	    }
	    public static void printNos(int initial, int last)
	    {
	        if (initial <= last) {
	            System.out.print(initial + " ");
	            printNos(initial + 1, last);
	        }
	    }
	}

